# README #

### What is this repository for? ###

* This repository contains the code of the Laminart model in its current NRP version (it will be updated with the new version of the Laminart model when ready) and all the transfer functions that are necessary for the model's use.

### How do I get set up? ###

* Download the repository somewhere on your computer (git clone https://albornet@bitbucket.org/albornet/laminartnrp.git).
* From this repository, put the "TransferFunctions" folder inside your NRP experiment folder.
* Their purpose is to feed the Laminart with the camera of the iCub, and to display some layers activity of the Laminart network in the generic image viewer, once the experiment is launched and played.
* You can of course edit them for your own purpose.
* Put the Laminart.py file inside $HBP/Models/brain_model/
* Also make the references to the brain file and the transfer functions in your .bibi file.
* A minimal example of .bibi file is provided if needed.

### Model parameters ###

* All parameters are defined at the beginning of the Laminart.py file.
* To set the size of the input, refer to lines 26 and 27. This will affect the simulation speed and the setup time. 

### Who do I talk to? ###

* Repository owner: alban.bornet@epfl.ch